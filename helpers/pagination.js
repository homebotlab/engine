exports.getPaginationOptions = function(req) {
  const page = (req.query.page !== undefined && !isNaN(req.query.page)) ? parseInt(req.query.page) : 1;
  const limit = (req.query.pageSize !== undefined && !isNaN(req.query.pageSize)) ? parseInt(req.query.pageSize) : 50;

  return {
    page: page,
    limit: limit
  };
};

exports.setPaginationHeaders = function(res, result) {
  res.set('Pagination-Count', result.total);
  res.set('Pagination-Page', result.page);
  res.set('Pagination-Limit', result.limit);
};
