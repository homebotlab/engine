const mongoose = require('mongoose');

exports.catchErrors = (fn) => {
  return (req, res, next) => {
    return fn(req, res, next).catch(next);
  }
}

exports.notFound = (req, res, next) => {
  const err = new Error('Not Found');
  res.status = 404;
  next(err);
}

exports.developmentErrors = (err, req, res, next) => {
  console.log(err);
  res.json({
    status: res.status,
    message: { level: 'error', text: err.message }
  });
};

// exports.productionErrors = (err, req, res, next) => {
//   res.json({
//     status: res.status,
//     message: { level: 'error', text: err.message }
//   });
// };