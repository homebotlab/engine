const { createError } = require("apollo-errors");

exports.BadRequestError = createError("Bad request", {
    message: ""
});

exports.UnauthorizedError = createError("Unauthorized", {
    message: ""
});

exports.ForbiddenError = createError("Forbidden", {
    message: ""
});

exports.NotFoundError = createError("Not Found", {
    message: ""
});

exports.ServerError = createError("Internal Server Error", {
    message: ""
});
