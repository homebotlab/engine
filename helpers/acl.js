const AccessControl = require('accesscontrol')
const modules = require('./modules');

/**
 * Getting the lastest grant list and
 * creation based on them AccessControl entity
 */
module.exports = () => {
    const grantList = modules.getGrantList();
    const acl = new AccessControl(grantList);

    return acl;
}