const { lstatSync, readdirSync, readFileSync, writeFile, mkdir } = require("fs");
const { promisify } = require("util");
const { join, relative, dirname } = require("path");
const Git = require("nodegit");

const writeFilePromisify = promisify(writeFile);
const mkdirPromisify = promisify(mkdir);

const isDirectory = src => lstatSync(src).isDirectory();

const getDirectories = src =>
    readdirSync(src)
    .map(name => join(src, name))
    .filter(isDirectory);

const getManifestFile = src => {
    const dirFiles = readdirSync(src);
    for (const file of dirFiles) {
        if (file === 'manifest.json') {
            return join(src, file);
        }
    }
}

const findSameGrant = (grants, { role, resource, action }) => {
    return grants.find(grant => {
        return grant.role === role 
            && grant.resource === resource 
            && grant.action === action;
    });
}

const findIndexSameGrant = (grants, { role, resource, action }) => {
    return grants.findIndex(grant => {
        return grant.role === role 
            && grant.resource === resource 
            && grant.action === action;
    });
}

module.exports = class Configurator {

    loadModules() {
        const dirCoreModules = join(__dirname, '../../');
        const dirLoadingModules = join(__dirname, '../../../../app/modules/');

        // loading module`s directories, 
        // last higher priority
        const directories = [
            ...getDirectories(dirCoreModules),
            ...getDirectories(dirLoadingModules)
        ];

        const manifestSrc = [];
        directories.forEach(src => {
            const manifestFile = getManifestFile(src);
            if (manifestFile) manifestSrc.push(manifestFile);
        });

        // redefinition of modules with same _id
        const manifestFiles = {};
        manifestSrc.forEach(src => {
            const data = JSON.parse(readFileSync(src, 'utf8'));
            const manifestKey = data._id;
            const manifestValue = {
                meta: {
                    src
                },
                data: data
            }

            if (manifestFiles[manifestKey]) {
                // if the manifest files in app/modules 
                // contains isActive key then use this module as executable
                // if exist manifest of app/modules then use this grants

                if (data.isActive) {
                    manifestFiles[manifestKey] = {
                      ...manifestValue,
                      data
                    };
                } else {
                    manifestFiles[manifestKey].data.grants = [
                      ...manifestFiles[manifestKey].data.grants,
                      ...data.grants
                    ];
                }

            } else {

                manifestFiles[manifestKey] = manifestValue;
                manifestFiles[manifestKey].data.grants = manifestFiles[
                  manifestKey
                ].data.grants
                  ? manifestFiles[manifestKey].data.grants.map(grant => {
                      return {
                        ...grant,
                        isBasic: true
                      };
                    })
                  : [];
            }
        });

        this.manifestFiles = Object.values(manifestFiles);
   }

    getGrantList() {
        const grantList = [];
        this.manifestFiles.forEach(manifest => {
            if(manifest.data.grants)
                grantList.push(...manifest.data.grants);
        });
        return grantList;
    }

    getRoleList() {
        const roleList = new Set();
        const grantList = this.getGrantList();
        
        grantList.forEach(grant => {
            roleList.add(grant.role);
        });

        return [...roleList];
    }

    getResourceList() {
        const resourceList = new Set();
        const grantList = this.getGrantList();

        grantList.forEach(grant => {
            resourceList.add(grant.resource);
        });

        return [...resourceList];
    }

    getModules() {
        return this.manifestFiles.map(({
            meta: {
                src
            },
            data: {
                _id,
                title,
                description,
                grants,
                tiles,
                staticPaths,
                routers,
                replicationClass,
                lastCommit
            }
        }) => {
            /**
             * Decorate modules list:
             *   - Remove grants with attribute equal "!"
             *   - Remove grants that are replaced by 
             *     app/manifest.json grants
             */

            const moduleGrants = grants ? [...grants]: [];
            const decoratedGrants = [];

            for (const grant of moduleGrants) {
                const sameGrantIdx = findIndexSameGrant(decoratedGrants, {
                    ...grant
                });

                if (sameGrantIdx !== -1) {
                    if (grant.attributes === "!") {
                        decoratedGrants.splice(sameGrantIdx, 1);
                    } else {
                        decoratedGrants[sameGrantIdx] = grant;
                    }
                } else {
                    decoratedGrants.push(grant)
                }
            }

            return {
                _id,
                src,
                title,
                description,
                grants: decoratedGrants,
                tiles,
                staticPaths,
                routers,
                replicationClass,
                lastCommit
            }
        });
    }

    getModule(_id) {
        return this.manifestFiles.find(item => item.data._id == _id);
    }

    getModel(model) {
        const modelMap = {};
        for (const manifest of this.manifestFiles) {
            if (manifest.data.models) {
                for (const model of manifest.data.models) {
                    modelMap[model.name] = join(
                       manifest.meta.src, "../", model.path
                    );
                }
            }
        }
        return modelMap[model];
    }

    async loadGitInfo() {
        for (const manifest of this.manifestFiles) {
            const { meta: { src }} = manifest;

            try {
                const gitRepo = await Git.Repository.open(join(src, "../.git"));
                const headCommit = await gitRepo.getHeadCommit();
                const commitHash = headCommit.toString().slice(0, 7);
                manifest.data.lastCommit = commitHash;
                
            } catch (err) {
                // ignoring no existing .git repositories
            }
        }
    }

    async updateModule(_id, data) {
        const manifestFile = this.manifestFiles.find(
          file => file.data._id === _id
        );

        if (manifestFile) {
            
            // create template for app/manifest.json file
            const newAppManifest = Object.assign({}, manifestFile.data, data, {
                grants: []
            });
            
            const prevGrants = manifestFile.data.grants;
            const newGrants = data.grants;   

            /**
             * Remove previous grants of app/manifest.json,
             * Detect updating && creating grants, adding them
             * to new app/manifest.json
             */
            
            for (const grant of newGrants) {
                const sameGrantIndex = findIndexSameGrant(prevGrants, {
                  ...grant
                });
                const sameGrant = prevGrants[sameGrantIndex];

                if (sameGrantIndex !== -1) {

                    // add grant to app manifest if grant is updated
                    if (grant.attributes !== sameGrant.attributes) {
                        newAppManifest.grants.push({...grant});
                        prevGrants.splice(sameGrantIndex, 1);

                    } else {

                        // return to app/manifest.json previuos app/manifest.json grants
                        if (!sameGrant.isBasic) {
                            newAppManifest.grants.push({...grant});
                            prevGrants.splice(sameGrantIndex, 1);
                        }
                    }

                } else {
                    // add grant to app/manifest.json if grant was added
                    newAppManifest.grants.push({...grant});
                }
            }

            /**
             * Detect deleting grants 
             * and adding them to app/manifest.json with
             * attributes = "!" that is equivalent to lack of access
            */
            
            for (const prevGrant of prevGrants) {
                const sameGrant = findSameGrant(newGrants, { ...prevGrant });
                
                if (!sameGrant) {
                    if (!findSameGrant(newAppManifest.grants, {...prevGrant})) {
                        newAppManifest.grants.push({
                            ...prevGrant,
                            attributes: "!"
                        });    
                    }
                }
            }

            // saving a new list of grants for the current module
            manifestFile.data.grants = [
              ...manifestFile.data.grants.filter(
                grant => grant.attributes !== "!"
              ),
              ...newAppManifest.grants
            ];           

            // remove temporary props of grant before saving
            newAppManifest.grants = newAppManifest.grants.map(grant => {
                return {
                    role: grant.role,
                    resource: grant.resource,
                    action: grant.action,
                    attributes: grant.attributes
                }
            });

            const appManifestStringnify = JSON.stringify(newAppManifest, null, 4);
            const projectPath = join(manifestFile.meta.src, '../../../../');
            const appManifestFilePath = join(
              projectPath,
              "app",
              relative(join(projectPath, "./src"), manifestFile.meta.src)
            );
            await mkdirPromisify(dirname(appManifestFilePath), { recursive: true });
            await writeFilePromisify(appManifestFilePath, appManifestStringnify, 'utf-8');

            return {
                _id,
                ...data,
            };

        } else {
            throw new Error(`Module with _id = ${_id} is not found`);
        }
    }
}