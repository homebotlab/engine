const { lstatSync, readdirSync, readFileSync } = require('fs');
const { join } = require("path");
const ini = require('ini');

const isDirectory = src => lstatSync(src).isDirectory();

const getDirectories = src =>
    readdirSync(src)
        .map(name => join(src, name))
        .filter(isDirectory);

class I18n {

    constructor(config) {
        this.locale = config.locale || '';
        this.directory = config.directory;
        this.configFiles = this._getConfigFiles();
        this.dictionary = {};

        this.configFiles.forEach(file => {
            this.dictionary = {
                ...this.dictionary,
                ...this._loadTranslationDict(file)
            }
        });
    }

    __(word, ...params) {
        if (this.dictionary[word]) {
            let result = this.dictionary[word];

            // replace {0} construction
            for (let i = 0; i < params.length; i++) {
                const replaceRegex = new RegExp(`\\{${i}\\}`, "g");
                result = result.replace(replaceRegex, params[i]);
            }

            // replace remainig and return
            return result.replace(/\{\d+\}/g, "");

        } else {
            return word;
        }
    }

    _loadTranslationDict(src) {
        return ini.parse(readFileSync(src, 'utf-8'));
    }

    _getConfigFiles() {
        const configFiles = [];
        const directories = getDirectories(this.directory);

        directories.forEach(src => {
            const configFile = this._getConfigFile(src);
            if (configFile) configFiles.push(configFile);
        });

        return configFiles;
    }

    _getConfigFile(src) {
        const dirFiles = readdirSync(src);
        const re = new RegExp(`^\\w*${this.locale}.properties$`, 'i');
        for (const file of dirFiles) {
            if (re.test(file)) {
                return join(src, file);
            }
        }
    }

}

module.exports = I18n;