const pagination = require('./pagination');
const sorting = require('./sorting');

exports.getRequestOptions = function(req) {
  const paginationOptions = pagination.getPaginationOptions(req);
  const sortOptions = sorting.getSortingOptions(req);

  return {...paginationOptions, ...sortOptions};
};

exports.getFilteringOptions = function(req) {
  return req.query.filter ? JSON.parse(req.query.filter) : {};
};
