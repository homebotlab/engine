const Configurator = require('./Configurator');

const configurator = new Configurator();
configurator.loadModules();
configurator.loadGitInfo();

module.exports = configurator;