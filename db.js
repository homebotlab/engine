const mongoose = require('mongoose');
const log = require('log4js').getLogger('mongoose');

const mongooseOptions = {
    'useNewUrlParser': true,
    'useCreateIndex': true,
    'useFindAndModify': false
};

mongoose.connect(process.env.DATABASE, mongooseOptions);
mongoose.Promise = global.Promise;

mongoose.connection.on('error', function (err) {
    log.error(err);
    console.error(`DB ERROR: ${err.message}`);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
    log.info('Mongoose default connection disconnected');
    console.log('Mongoose default connection disconnected'); 
});
  
// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
      log.info('Mongoose default connection disconnected through app termination');
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0); 
    }); 
}); 