const express = require('express');
const session = require('express-session');
const flash = require('connect-flash');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const path = require('path');
const log4js = require('log4js');
const response = require(`${process.env.HELPERS_PATH}/response`);
const configurator = require(`${process.env.HELPERS_PATH}/modules`);
const modules = configurator.getModules();
const log = log4js.getLogger("app");

const {
  developmentErrors,
  notFound
} = require('./helpers/errors');

const app = express();

app.use(bodyParser.json({
  limit: '10mb'
}))
app.use(bodyParser.urlencoded({
  limit: '10mb',
  extended: true
}))

app.use(session({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    collection: 'userSession'
  })
}));

app.use(flash());

const passport = require('passport');

app.use(passport.initialize());
app.use(passport.session());

require('../../config/passport')(passport);
require('../../config/logger')(log4js);

app.use(log4js.connectLogger(log4js.getLogger("http"), { level: "auto" }));

// app.use(notFound);

// if (process.env.NODE_ENV === 'development') {
//   app.use(developmentErrors);
// }

// static
const routes = express.Router();

routes.use(response.setHeadersForCORS);

app.use('/resources', express.static(path.resolve(process.env.SDK_PATH)));

modules.forEach(oModule => {
		const modulePath = path.resolve(oModule.src, "..");
		log.info(`Connected module ${oModule._id} by path ${modulePath}`);
		
  	if (oModule.staticPaths) {
    	oModule.staticPaths.forEach(static => {
			app.use(
				'/modules/' + oModule._id + static.url,         // brouser url
				express.static(
					path.join(modulePath, static.url)
				)
			);
    	});
  	}
	if (oModule.routers) {
		oModule.routers.forEach(r => {
			let rp = path.join(modulePath, r.path);
			routes.use(r.url, require(rp));
			//console.log(`${r.url} -> ${rp}`);
		});
	}
});

app.use('/', routes);

module.exports = app;