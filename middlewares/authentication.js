const mongoose = require('mongoose');
const response = require('../helpers/response');

const User = mongoose.model('User');

module.exports = function authenticationMiddleware() {
  return async function (req, res, next) {
    if (req.isAuthenticated()) {
      await User.findByIdAndUpdate(req.user._id, { last_seen_dttm: new Date() });
      return next()
    }
    return response.sendUnauthorized(res, 'Authentication failed.');
  }
}
