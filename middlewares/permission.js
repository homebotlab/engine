const response = require('../helpers/response');
const acl = require('../helpers/acl');

exports.readOwn = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).readOwn(resource);
    resolve(req, res, next, permission);
  }
}

exports.createOwn = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).createOwn(resource);
    resolve(req, res, next, permission);
  }
}

exports.updateOwn = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).updateOwn(resource);
    resolve(req, res, next, permission);
  }
}

exports.deleteOwn = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).deleteOwn(resource);
    resolve(req, res, next, permission);
  }
}

exports.allOwn = (resource) => {
  return (req, res, next) => {
    const permission =
      acl().can(req.user.role).readOwn(resource) ||
      acl().can(req.user.role).createOwn(resource) ||
      acl().can(req.user.role).updateOwn(resource) ||
      acl().can(req.user.role).deleteOwn(resource);
    resolve(req, res, next, permission);
  }
}

exports.readAny = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).readAny(resource);
    resolve(req, res, next, permission);
  }
}

exports.createAny = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).createAny(resource);
    resolve(req, res, next, permission);
  }
}

exports.updateAny = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).updateAny(resource);
    resolve(req, res, next, permission);
  }
}

exports.deleteAny = (resource) => {
  return (req, res, next) => {
    const permission = acl().can(req.user.role).deleteAny(resource);
    resolve(req, res, next, permission);
  }
}

exports.allAny = (resource) => {
  return (req, res, next) => {
    const permission =
      acl().can(req.user.role).readAny(resource) ||
      acl().can(req.user.role).createAny(resource) ||
      acl().can(req.user.role).updateAny(resource) ||
      acl().can(req.user.role).deleteAny(resource);
    resolve(req, res, next, permission);
  }
}

function resolve(req, res, next, permission) {
  if (permission.granted) {
    req.permission = permission;
    next();
  } else {
    response.sendForbidden(res);
  }
}


