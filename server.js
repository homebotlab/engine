const path = require("path");
const log = require('log4js').getLogger("server");
require('dotenv').config();

process.env.HELPERS_PATH = path.resolve(__dirname, "./helpers");
process.env.MIDDLEWARES_PATH = path.resolve(__dirname, "./middlewares");

const app = require('./app');
const db = require('./db');
app.set('port', process.env.PORT);

const server = app.listen(app.get('port'), () => {
  log.info(`Server running at PORT ${server.address()['port']}`);
  log.info(`environment: ${process.env.NODE_ENV}`);  
  console.log(`Server running at PORT ${server.address()['port']}`)
  console.log(`environment: ${process.env.NODE_ENV}`);
});